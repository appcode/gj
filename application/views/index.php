<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>卧龙团队</title>
		<meta name="Keywords" content="时时彩 挂机 自动投注 计划">
		<meta name="description" content="卧龙团队定位于时时彩挂机方案研究，旗下代理经纬、拉菲、游艇会等平台，为玩家提供稳定的平台，稳定的挂机软件，稳定的挂机投注方案，减少手动投注的干扰，提升玩家盈利水平！">

		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resource/css/style.css');?>">
		<script type="text/javascript" src="http://libs.baidu.com/jquery/1.9.1/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url('resource/js/main.js');?>"></script>
	</head>
	<body>
		<div id="short_cut">
			<div class="w">
				<div align="center">
					<?php foreach ($h_header as $id => $h_item){?>
					<a><?php echo $h_item->h_text?></a>
					<span class="spacer"></span>
					<strong>
						<span class="w1"><?php echo $h_item->h_qq?></span>
					</strong>
					<?php }?>
				</div>
			</div>
		</div>

		<div class="index_head">
			<a id="logo" href="#" title="卧龙团队">
				<img src="<?php echo base_url('resource/images/teamlogo.png');?>" alt="卧龙团队">
			</a>
			<h1>卧龙团队</h1>
		</div>

		<div class="index_nav">
			<div id="nav">
				<ul>
					<li id="dhindex">
						<a class="navi_1" href="javascript:void(0);">首页</a>
					</li>
					<li>
						<a class="navi_1" href="javascript:void(0);">软件下载</a>
						<div class="navi_item" style="visibility:hidden;">
							<?php foreach ($s_platform as $id => $p_item){?>
							<a href="<?php echo $p_item->p_download ?>" target="__target"><?php echo $p_item->p_name?>挂机软件</a>
							<?php }?>
						</div>
					</li>
					<li>
						<a class="navi_1" href="javascript:void(0);">平台注册</a>
						<div class="navi_item" style="visibility: hidden;">
							<?php foreach ($s_platform as $id => $p_item){?>
							<a href="<?php echo $p_item->p_reg?>" target="__target"><?php echo $p_item->p_name?>注册</a>
							<?php }?>
						</div>
					</li>
				</ul>
			</div>
		</div>

		<div class="index_banner">
			<ul class="banner_content">
			
				<?php foreach ($b_banner as $id => $b_banner_item){?>
				<li class="banner_panel">
					<a class="b1" href="<?php echo $b_banner_item->b_url?>">
						<img style="margin: 0 auto;" src="<?php echo $b_banner_item->b_image?>">
					</a>
				</li>
				<?php }?>
			
			</ul>
			<ul class="banner_nav">
				<?php foreach ($b_banner as $id => $b_banner_item){?>
				<li class="banner_trigger"><?php echo $id + 1?></li>
				<?php }?>
			</ul>
		</div>

		<div class="wide-wrap">
			<div class="clearfix">
				<div class="item-title">
					<h2>自动投注软件特性</h2>
				</div>
				<div class="soft-feature">
					<ul class="list">
						<li class="list-item">
							<span class="item-list-title">支持平台</span>
							<ul>					
								<?php foreach ($s_platform as $id => $p_item){?>
								<li>
									<div>
										<strong><?php echo $p_item->p_name?></strong>平台:
									</div>
									<span class="plat-s1"><?php echo $p_item->p_type?></span>									
									<a href="<?php echo $p_item->p_reg?>" target="_target">[自动注册]</a>
								</li>
								<?php }?>
								
							</ul>
						</li>

						<li class="list-item">
							<span class="item-list-title">使用指南</span>
							<ul>
								<li>
									<span>软件设置及使用视频教程</span>
									<a href="http://www.baidu.com" target="_target">[下载地址]</a>
								</li>
								<li>
									<span>高级倍投设置视频教程</span>
									<a href="http://www.baidu.com" target="_target">[下载地址]</a>
								</li>
								<li>
									<span>挂机倍投方案集合</span>
									<a href="http://www.baidu.com" target="_target">[下载地址]</a>
								</li>
							</ul>
						</li>


						<li class="list-item">
							<span class="item-list-title">软件优势</span>
							<ul>
								<li>
									<span>加入团队，软件永久免费使用！</span>
								</li>
								<li>
									<span>解放双手，让软件代替人工打号！</span>
								</li>
								<li>
									<span>排除干扰，避免连挂对心理的影响！</span>
								</li>
							</ul>
						</li>

					</ul>

				</div>
			</div>

			<div class="platform-recomm">
				<div class="item-title">
					<h2>平台介绍</h2>
				</div>
				<div class="platform-list">
					<table class="p-tb" cellpadding="0" cellspacing="0">
						<thead>
							<tr class="tb-head">
								<th width="10%">平台名称</th>
								<th width="30%">平台简介</th>
								<th width="10%">平台模式</th>
								<th width="10%">奖金</th>
								<th width="30%">测速地址</th>
							</tr>
						</thead>
						
						<?php foreach ($s_platform as $id => $p_item){?>
						<tbody>
							<tr class="tb-h">
								<td colspan="5"></td>
							</tr>
							<tr>
								<td>
									<a href="<?php echo $p_item->p_reg?>" target="_target">
										<img src="<?php echo $p_item->p_logo?>" width="150" height="60" alt="<?php echo $p_item->p_name?>">
									</a>
								</td>
								<td>
									<?php echo $p_item->p_summary?>
								</td>
								<td>
									<span><?php echo $p_item->p_model?></span>
								</td>
								<td>
									<span><?php echo $p_item->p_bonus?></span>
								</td>
								<td>
									<a href="<?php echo $p_item->p_speed_test?>" target="_target">测速</a>
								</td>
							</tr>
							<tr class="tb-h border">
								<td colspan="5"></td>
							</tr>
						</tbody>
						<?php }?>

					</table>
				</div>
			</div>

		</div>	

		<div class="warm-tips" align="center">
			<small>彩票投资有风险，投资需谨慎(团队服务时间：10：00-24:00)</small>
		</div>	

	</body>
</html>