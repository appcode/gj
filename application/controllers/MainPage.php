<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MainPage extends CI_Controller{
    
    function __construct()
    {
        parent::__construct();
        
        $this->load->model('platform');
        $this->load->model('banner');
        $this->load->model('header');
    }
    
    public function index(){
        $result = $this->header->select_header();
        $data['h_header'] = $result;
//         var_dump($result);
        
        $result = $this->platform->select_platform();
        $data['s_platform'] = $result;
        
        $result = $this->banner->select_banner();
        $data['b_banner'] = $result;
        
        $this->load->helper('url');
        $this->load->view('index',$data);
    }
}