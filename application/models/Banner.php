<?php
class Banner extends CI_Model{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    function select_banner() {
        $this->db->where('valid',1);
        $this->db->select('b_image,b_url');
        $query = $this->db->get('tb_banner');
        $result = $query->result();
        
        return $result;
    }
}