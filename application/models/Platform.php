<?php
class Platform extends CI_Model{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    function select_platform() {
        $this->db->where('valid',1);
        $this->db->select('p_name,p_logo,p_summary,p_type,p_model,p_bonus,p_speed_test,p_reg,p_download');
        $query = $this->db->get('tb_platform');
        $result = $query->result();
        
        return $result;
    }
}