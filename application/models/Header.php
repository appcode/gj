<?php
class Header extends CI_Model{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    function select_header() {
        $this->db->where('valid',1);
        $this->db->limit(1);
        $this->db->select('h_text,h_qq');
        $query = $this->db->get('tb_header');
        $result = $query->result();
        
        return $result;
    }
}