
$(document).ready(function(){
	//轮播广告
	var t = setInterval("timeCount()",5000);
	
	$(".banner_content").find(".banner_panel:first").addClass("panel_active");
	$(".banner_nav").find(".banner_trigger:first").addClass("trigger_active");
	
	$(".banner_nav").find("li").click(function(){
		clearInterval(t);	
		var index = $(this).index();
		$(".panel_active").removeClass("panel_active");
		$($(".banner_panel")[index]).addClass("panel_active");
		$(".trigger_active").removeClass("trigger_active");
		$(this).addClass("trigger_active");
		t = setInterval("timeCount()",5000);
	});
	
	//绑定事件
	$('#nav li').bind('mouseenter',function(){
		$(this).find('.navi_item').css('visibility','visible');
	});
	$('#nav li').bind('mouseleave',function(){
		$(this).find('.navi_item').css('visibility','hidden');
	});
	
});

function timeCount() {
	var total = parseInt($(".banner_panel").length);
	
	var next_panel = $(".panel_active").next();
	if($(".panel_active").index() == total - 1) {
		next_panel = $(".banner_panel")[0];
	}
	$(".panel_active").removeClass("panel_active");
	$(next_panel).addClass("panel_active");
	
	var next_trigger = $(".trigger_active").next();
	if($(".trigger_active").index() == total - 1) {
		next_trigger = $(".banner_trigger")[0];
	}
	$(".trigger_active").removeClass("trigger_active");
	$(next_trigger).addClass("trigger_active");
	
}