-- phpMyAdmin SQL Dump
-- version 3.3.10.5
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2016 年 06 月 18 日 09:31
-- 服务器版本: 5.5.49
-- PHP 版本: 5.4.45-3+donate.sury.org~precise+3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `uu184338`
--

-- --------------------------------------------------------

--
-- 表的结构 `tb_platform`
--

CREATE TABLE IF NOT EXISTS `tb_platform` (
  `id` int(11) DEFAULT NULL,
  `p_name` varchar(96) DEFAULT NULL,
  `p_logo` varchar(384) DEFAULT NULL,
  `p_summary` varchar(3072) DEFAULT NULL,
  `p_type` varchar(384) DEFAULT NULL,
  `p_model` varchar(96) DEFAULT NULL,
  `p_bonus` int(11) DEFAULT NULL,
  `p_speed_test` varchar(3072) DEFAULT NULL,
  `p_reg` varchar(3072) DEFAULT NULL,
  `p_download` varchar(128) NOT NULL COMMENT '下载链接',
  `valid` int(11) DEFAULT NULL,
  `sequenct` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tb_platform`
--

INSERT INTO `tb_platform` (`id`, `p_name`, `p_logo`, `p_summary`, `p_type`, `p_model`, `p_bonus`, `p_speed_test`, `p_reg`, `p_download`, `valid`, `sequenct`) VALUES
(1, '游艇会', 'http://d2.freep.cn/3tb_160615234003w1eh566734.png', '游艇会平台隶属于亚泰旗下新力作，资金安全保障，秒冲秒提！支付宝微信充值接口近期已经上线。平台彩种丰富，不设置平台自主彩种，均为官方采集开奖号码。彩票类彩种包括:《韩国1.5分彩，重庆，天津，江西，新疆时时彩，北京PK10等等》更有真人娱乐等等。1厘模式，等你体验！', '重庆彩/香港5分彩/韩国1.5分彩', '元角分厘', 1950, 'http://www.qqsxy.com/duobao/ut8/ut8.html', 'http://m3.uth8888.com/view/game/register.html?id=8fe58ff1-8b2d-452e-ab38-efc934aa64e5', 'http://pan.baidu.com/s/1o8xTtzG', 1, 1),
(2, '经纬', 'http://d2.freep.cn/3tb_1606152340022ft5566734.png', '老平台,玩家多,稳定,充值、返奖速度快,客服态度好！', '重庆彩/分分彩/三分彩', '元角分厘', 1940, 'http://denglu.jingwei129.com', 'http://www.nihao123.org/cpp/reg?info=PJACGKIWL&f=%C6%BD%CC%A8%D7%D4%D6%FA%D7%A2%B2%E1', 'http://pan.baidu.com/s/1boIsnk7', 1, 2),
(3, '拉菲', 'http://d3.freep.cn/3tb_1606152340024gjr566734.png', '拉菲娱乐平台注册资本1.19亿元，2016行业最有信誉的票投注平台，集合了当前最新的时时彩玩法，稳定服务线路。平台存 取超快!安全有保障，提供24小时在线服务，是国内行业内最大的第三方担保团队。', '重庆彩/韩国1.5分彩', '元角分厘', 1950, 'http://jslcc.romall.net', 'http://t.cn/R5xoKvR', 'http://pan.baidu.com/s/1boIsnk7', 1, 3);
