-- phpMyAdmin SQL Dump
-- version 3.3.10.5
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2016 年 06 月 18 日 09:31
-- 服务器版本: 5.5.49
-- PHP 版本: 5.4.45-3+donate.sury.org~precise+3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `uu184338`
--

-- --------------------------------------------------------

--
-- 表的结构 `tb_header`
--

CREATE TABLE IF NOT EXISTS `tb_header` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `h_text` varchar(256) DEFAULT NULL,
  `h_qq` varchar(128) DEFAULT NULL,
  `valid` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `tb_header`
--

INSERT INTO `tb_header` (`id`, `h_text`, `h_qq`, `valid`) VALUES
(1, '卧龙团队于2015年成立，合作平台包括经纬、拉菲、游艇会等平台,玩家资金全程担保!', '开户联系QQ:79553076', 1);
