-- phpMyAdmin SQL Dump
-- version 3.3.10.5
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2016 年 06 月 18 日 09:31
-- 服务器版本: 5.5.49
-- PHP 版本: 5.4.45-3+donate.sury.org~precise+3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `uu184338`
--

-- --------------------------------------------------------

--
-- 表的结构 `tb_banner`
--

CREATE TABLE IF NOT EXISTS `tb_banner` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `b_name` varchar(64) DEFAULT NULL COMMENT 'banner名称',
  `b_image` varchar(128) DEFAULT NULL COMMENT 'banner图片链接',
  `b_url` varchar(128) DEFAULT 'javascript:void(0);' COMMENT '点击链接',
  `valid` int(11) DEFAULT '1' COMMENT '是否有效 1 有效 0 无效',
  `b_summary` varchar(256) DEFAULT NULL COMMENT '摘要内容',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `tb_banner`
--

INSERT INTO `tb_banner` (`id`, `b_name`, `b_image`, `b_url`, `valid`, `b_summary`) VALUES
(1, '游艇会', 'http://d3.freep.cn/3tb_160615234002qh12566734.png', 'javascript:void(0);', 1, NULL),
(2, '经纬', 'http://d2.freep.cn/3tb_160615234000mg21566734.png', 'javascript:void(0);', 1, NULL),
(3, '拉菲', 'http://d2.freep.cn/3tb_160615234001x6ec566734.png', 'javascript:void(0);', 0, NULL),
(4, '挂机', 'http://d3.freep.cn/3tb_160615234000onsx566734.png', 'javascript:void(0);', 1, NULL);
